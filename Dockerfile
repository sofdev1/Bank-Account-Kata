FROM openjdk:13-alpine
Volume /tmp
ADD /target/*.jar Bank-Account-Kata-0.0.1-SNAPSHOT.jar
ENV PORT 8081
EXPOSE 8081
ENTRYPOINT ["java","-jar","/Bank-Account-Kata-0.0.1-SNAPSHOT.jar"]  